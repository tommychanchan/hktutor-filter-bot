# -*- coding: UTF-8 -*-

import requests
import urllib3
import os
import json
import sys
import bs4
import time
import urllib.robotparser


rp = urllib.robotparser.RobotFileParser()
rp.set_url("https://www.hktutor.hk/robots.txt")
rp.read()

urllib3.disable_warnings()


VERSION = "1.2.1"

configFile = "config.txt"
sampleConfigFile = "sample-config.txt"
config = {}
outputFile = "output.txt"
finish = False
debugMode = False

# for counting number of matches case
count = 0

startDefault = 1
endDefault = 5


configText = """# 這是設定檔，你可以設定要篩選的內容，包括以下類別：
# 地點、科目、年級、性別、每小時工資。
# 請在每個類別以下加入文字指示要保留的結果。
# (如果無需篩選這個類別，則留空)
# 可忽略大小寫，因此 "Zoom" 和 "zoom" 是一樣的。
# 以 "#" 符號開頭的是註解，讀入此檔案時程式會無視這些行。


[地點]

銅鑼灣
天水圍
大埔

# 有些 家長/學生 會在地點填網上教學
zoom
online


[科目]

中文
英文
數學

# 其他科目 (我從HKTUTOR網站的一些個案例子copy的，移除 "#" 號就可以啟用)：
#全科
#通識
#常識
#經濟
#會計
#企業概論
#科學
#物理
#化學
#生物
#歷史
#世界歷史
#英國文學
#英語會話
#IELTS
#英文朗誦
#普通話
#心理學
#Psychology
# 我都唔知點解M1有個空格 -_-
#M 1
#鋼琴
#畫畫
#素描及油畫


[年級]

#學前
#小一
#小二
#小三
#小四
#小五
#小六
#中一
#中二
#中三
#中四
#中五
#中六
#大專


[性別]

#男
#女


[每小時工資]

# 如果每小時工資有多過1個條件，程式只會用最後1個條件
# 以下是一些例子：

# $60 至 $300 (包括 $60 及 $300)
#60-300

# 少於或等於 $200
#-200

# 大於或等於 $50
#50-

"""






def search(page):
	global config
	global finish
	global count
	url = "https://www.hktutor.hk/?pageNum=" + str(page)
	if not rp.can_fetch("*", url):
		print("Error: \"" + url + "\" is blocked by robots.txt!")
		return ""


	headers = {
		"User-Agent": "Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.142 Safari/537.36 OPR/62.0.3331.116"
	}

	debug()
	debug()
	debug("Trying to access - " + url)
	print("Progress: Page " + str(page) + ".")

	try:
		result = requests.get(url, timeout = 40, headers = headers, verify = False).text
	except:
		print("Error: Cannot access page " + str(page) + " of the website.")
		return ""

	result = result.encode().decode('UTF-8')

	output = ""

	if "只可查詢一個月內的補習個案" in result:
		# too far away
		finish = True
		return output


	root = bs4.BeautifulSoup(result, "html.parser")

	mainDiv = root.find_all("div", class_ = "center")[0]

	# for storing each case's data (e.g. place, subject, gender)
	data = {}

	# for storing if each case's field meet the filter requirement
	match = {}

	root = bs4.BeautifulSoup(str(mainDiv), "html.parser")
	tables = root.find_all("table")
	for table in tables:
		data.clear()
		match.clear()
		match["place"] = False
		match["subject"] = False
		match["grade"] = False
		match["gender"] = False
		match["salary"] = False

		root2 = bs4.BeautifulSoup(str(table), "html.parser")

		data["place"] = root2.find_all("tr")[0].find_all("td")[0].text.replace("\n", "").strip() + \
				" " + root2.find_all("tr")[0].find_all("td")[1].text.replace("\n", "").strip()

		data["subject"] = root2.find_all("tr")[1].find_all("td")[0].text.replace("\n", "").strip()

		data["grade"] = root2.find_all("tr")[1].find_all("td")[1].text.replace("\n", "").strip().split(" ")[0]

		data["gender"] = root2.find_all("tr")[1].find_all("td")[1].text.replace("\n", "").strip().split(" ")[1].strip("()")

		data["salary"] = root2.find_all("tr")[2].find_all("td")[0].find_all("span")[0].text.replace("\n", "").strip(" $")

		if root2.find_all("tr")[3].find_all("td")[0].find(text=True, recursive=False) == None:
			data["remarks"] = "無"
		else:
			data["remarks"] = root2.find_all("tr")[3].find_all("td")[0].find(text=True, recursive=False).replace("\n", "").replace("\r", " ").strip()

		if data["remarks"] == "":
			data["remarks"] = "無"

		debug("data = " + str(data))

		# place
		if len(config["place"]) != 0:
			for i in config["place"]:
				if i in data["place"].lower():
					match["place"] = True
					break
		else:
			# no need to filter
			match["place"] = True

		# subject
		if len(config["subject"]) != 0:
			for i in config["subject"]:
				if i in data["subject"].lower():
					match["subject"] = True
					break
		else:
			# no need to filter
			match["subject"] = True

		# grade
		if len(config["grade"]) != 0:
			for i in config["grade"]:
				if i in data["grade"].lower():
					match["grade"] = True
					break
		else:
			# no need to filter
			match["grade"] = True

		# gender
		if len(config["gender"]) != 0:
			for i in config["gender"]:
				if i in data["gender"].lower():
					match["gender"] = True
					break
		else:
			# no need to filter
			match["gender"] = True

		# salary
		if len(config["salary"]) != 0:
			if config["salary"][-1].split("-")[0] == "":
				lowerSalary = -1
			else:
				lowerSalary = float(config["salary"][-1].split("-")[0])

			if config["salary"][-1].split("-")[1] == "":
				upperSalary = -1
			else:
				upperSalary = float(config["salary"][-1].split("-")[1])

			if lowerSalary == -1 and upperSalary == -1:
				# no need to filter
				match["salary"] = True
			elif lowerSalary == -1:
				# check if <= upperSalary
				if float(data["salary"]) <= upperSalary:
					match["salary"] = True
			elif upperSalary == -1:
				# check if >= lowerSalary
				if float(data["salary"]) >= lowerSalary:
					match["salary"] = True
			else:
				# check if (lowerSalary <= data["salary"] <= upperSalary)
				if (float(data["salary"]) >= lowerSalary) and (float(data["salary"]) <= upperSalary):
					match["salary"] = True

			if i in data["salary"]:
				match["salary"] = True
				break
		else:
			# no need to filter
			match["salary"] = True

		if not (match["place"] and match["subject"] and match["grade"] and match["gender"]):
			continue

		count += 1

		link = root2.find("a")

		output += "--" + link['href'].split("id=")[-1] + "--"

		output += "\n地點：" + data["place"]
		output += "\n科目：" + data["subject"]
		output += "\n年級：" + data["grade"]
		output += "\n性別：" + data["gender"]
		output += "\n每小時工資：$" + data["salary"]
		output += "\n備註：" + data["remarks"]

		if link['href'].startswith("/"):
			output += "\nLink: https://www.hktutor.hk" + link['href']
		else:
			output += "\nLink: " + link['href']
		output += "\n\n"
	return output



def readConfig():
	global config
	config.clear()
	config["place"] = []
	config["subject"] = []
	config["grade"] = []
	config["gender"] = []
	config["salary"] = []

	if os.path.exists(configFile):
		f = open(configFile, "r", encoding = "utf-8")
		lines = f.read().split("\n")

		nowType = ""
		for line in lines:
			if line.startswith("#") or line.strip() == "":
				continue
			elif line == "[地點]":
				nowType = "place"
			elif line == "[科目]":
				nowType = "subject"
			elif line == "[年級]":
				nowType = "grade"
			elif line == "[性別]":
				nowType = "gender"
			elif line == "[每小時工資]":
				nowType = "salary"
			else:
				if nowType != "":
					config[nowType].append(line.strip().lower())

		debug("config = " + str(config))

		f.close()
	else:
		writeDefaultConfig()
		print("Config file \"" + configFile + "\" is created. You may edit it.")
		print("Please start the program again after you finished editing the config file.")
		input("Press <enter> to continue...")
		sys.exit(0)



def writeDefaultConfig():
	f = open(configFile, "w+", encoding = "utf-8")
	f.write(configText)
	f.close()
	debug("Created file \"" + sampleConfigFile + "\".")


def writeSampleConfig():
	f = open(sampleConfigFile, "w+", encoding = "utf-8")
	f.write(configText)
	f.close()
	debug("Created/Overwrote file \"" + sampleConfigFile + "\".")


def debug(msg = ""):
	if debugMode:
		if msg == "":
			print()
		else:
			print("DEBUG: " + str(msg))





if len(sys.argv) > 1:
	args = sys.argv[1:]
	debugMode = ("debug" in args)

	debug("args = " + str(args))

# updated version of config file
writeSampleConfig()


readConfig()

debug()

print("Version: " + VERSION)

while True:
	start = input("Start page(default: " + str(startDefault) + "): ")
	if start == "":
		start = startDefault
	try:
		start = int(start)
	except:
		print("Invalid input.")
	else:
		break

print()
print("Please note that end page don't be too far from start page.")
print("Otherwise, ip may be blocked because of too much requests.")


while True:
	end = input("End page(default " + str(endDefault) + "): ")
	if end == "":
		end = endDefault
	try:
		end = int(end)
	except:
		print("Invalid input.")
	else:
		break

if start > end:
	start, end = end, start


print()
print("Trying to access the website, please wait...")


output = ""

for i in range(start, end + 1):
	if finish:
		break
	output += search(i)
	time.sleep(1)


info = "篩選條件如下：\n"

if len(config["place"]) == 0:
	info += "地點：所有\n"
else:
	info += "地點：" + "，".join(config["place"]) + "\n"

if len(config["subject"]) == 0:
	info += "科目：所有\n"
else:
	info += "科目：" + "，".join(config["subject"]) + "\n"

if len(config["grade"]) == 0:
	info += "年級：所有\n"
else:
	info += "年級：" + "，".join(config["grade"]) + "\n"

if len(config["gender"]) == 0:
	info += "性別：所有\n"
else:
	info += "性別：" + "，".join(config["gender"]) + "\n"

if len(config["salary"]) == 0:
	info += "每小時工資：所有\n"
else:
	if config["salary"][-1].split("-")[0] == "" and config["salary"][-1].split("-")[1] == "":
		info += "每小時工資：所有\n"
	elif config["salary"][-1].split("-")[0] == "":
		info += "每小時工資：少於或等於 $" + config["salary"][-1].split("-")[1] + "\n"
	elif config["salary"][-1].split("-")[1] == "":
		info += "每小時工資：大於或等於 $" + config["salary"][-1].split("-")[0] + "\n"
	else:
		info += "每小時工資：" + config["salary"][-1] + "\n"


f = open(outputFile, "w+", encoding = "utf-8")
if count > 0:
	output = str(count) + " 個結果。\n\n" + info + "\n" + output
	f.write(output)
else:
	f.write(info + "\n沒有符合篩選的結果。\n")
f.close()


print()
print("Finished.")
print("You may now check the file \"" + outputFile + "\" to see the results.")

input("Press <enter> to continue...")
