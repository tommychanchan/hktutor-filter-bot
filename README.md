# HKTUTOR Filter Bot

[香港補習導師會（HKTUTOR）網站](https://www.hktutor.hk/)沒有篩選功能，因此我寫了這個bot去幫我自動篩選（只適用於導師尋找學生）。

## 下載  (Windows 執行檔)

[Link](https://drive.google.com/drive/folders/1kpkWJrJHX2eUcTMXa6h0DasnfnEBx6qj?usp=sharing)

第一次執行會創建設定檔 `config.txt`，修改此設定檔後請再次執行程式。


## 更新


下載最新版本後，設定檔可能需要更新，請先執行一次**新版本**的程式，然後參考(或複製) `sample-config.txt` 的格式並修改設定檔 `config.txt` 確保格式一致。

## 從source code執行 (不只限 Windows)

請確保你已安裝 [Python 3](https://www.python.org/downloads/).

安裝 Python 3 後請安裝需要的modules: `pip install -r requirements.txt`

### 執行

`python main.py`

Debug: `python main.py debug`

### 編譯 Windows 執行檔 (只適用於 Windows)

執行 `build.bat`

